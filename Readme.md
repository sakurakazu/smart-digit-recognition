# Smart Digit Recognition

## Requirements

* Tensorflow 1.12.0
* OpenCV 4.0.0
* TfLearn 0.3.2
* python 3.6.5

## Demo

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/DR_1.gif" alt="Video demo"/></p>

## Introduction

The Smart digit recognition application uses CNN to detect hand written **'Student IDs'** from images. The application is trained on **'MNIST handwritten'** digits and can recognize digits with high precision.

The predicted 'Student ID' is matched to a student database and retrieves student information. A distance measure **'hamming distance'** is used to match the predicted student ID with student IDs in database.

This application is a prototype implementation to auto grade papers. 

## Description

The CNN has two Convolutional layers, two max-pool layers and
two Fully Connected layers with dropout . The Neural network model structure is shown below. The first fully connected layer uses ’RELU’ as activation function and the second fully connected layer uses ’softmax’ activation function. The model is trained for 10,000 epochs using Adam optimizer till the Categorical cross-entropy reaches a stable optimal value of 0.08.

## Model

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/model.png" alt="Video demo"/></p>

## Sample inputs

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/sample_input.png" alt="Video demo"/></p>

## Threshold

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/thershold.png" alt="Video demo"/></p>

## Predicted digits from image

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/predictions.png" alt="Video demo"/></p>

## Predicted contours from image

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/predictions_1.png" alt="Video demo"/></p>

## Output

<p align="center"><img src="https://gitlab.com/sudheer2015/smart-digit-recognition/raw/master/images/output.png" alt="Video demo"/></p>


## Instructions to run the code

### Project directory structure

The project directory should be as follows.

```
-- data
    -- test
        -- handwritten student IDs
    -- student_information.txt
-- MNIST_data
    -- mnist dataset in ubyte.tar.gz format
-- TF_learn_checkpoint
    -- Saved checkpoint files
```

### Training

> python TF_learn_cnn_mnist.py

### Inference

> python performRecognition.py
