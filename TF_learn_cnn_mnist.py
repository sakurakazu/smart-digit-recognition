#!/usr/bin/python3
import numpy as np
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import dropout, fully_connected
from tflearn.layers.estimator import regression
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf


class TF_learn_cnn_mnist(object):
    def __init__(self):
        self.mnist = tf.examples.tutorials.mnist.input_data.read_data_sets("MNIST_data/", one_hot=True)

        self.X_train, self.Y_train = np.array(self.mnist.train.images), np.array(self.mnist.train.labels)
        self.X_test, self.Y_test = np.array(self.mnist.test.images), np.array(self.mnist.test.labels)

        self.epochs = 20
        self.learning_rate = 0.01
        # Total 6 batches or training data
        self.batch = 11000

        print(self.X_train.shape, self.Y_train.shape, self.X_test.shape, self.Y_test.shape)

    def model(self, reuse=False):
        convnet = tflearn.layers.core.input_data(shape=[None, 28, 28, 1], name='input')

        convnet = conv_2d(convnet, 32, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 64, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = fully_connected(convnet, 1024, activation='relu')
        convnet = dropout(convnet, 0.8)

        convnet = fully_connected(convnet, 10, activation='softmax')
        convnet = regression(convnet, optimizer='adam', learning_rate=self.learning_rate, loss='categorical_crossentropy',
                             name='targets')

        model = tflearn.DNN(convnet)

        if reuse:
            model.load('TF_learn_mnist_checkpoint/tflearn_model.model')

        return model

    def train(self, reuse=False):
        train_x = self.X_train.reshape([-1, 28, 28, 1])
        test_x = self.X_test.reshape([-1, 28, 28, 1])

        model = self.model(reuse)
        model.fit({'input': train_x}, {'targets': self.Y_train}, batch_size=self.batch, n_epoch=self.epochs, validation_set=({'input': test_x}, {'targets': self.Y_test}),
                  snapshot_step=self.batch, show_metric=True, run_id='mnist')

        model.save('TF_learn_mnist_checkpoint/tflearn_model.model')

    def test(self, image, reuse=False):
        image = image.reshape([-1, 28, 28, 1])
        model = self.model(reuse)
        predicted_labels = np.argmax(model.predict(image), axis=1)

        return predicted_labels


if __name__ == '__main__':
    obj = TF_learn_cnn_mnist()
    obj.train()
