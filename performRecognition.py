# Import the modules
import cv2
import sys
import numpy as np
import TF_learn_cnn_mnist
import operator

# Read the input image
im = cv2.imread("data/test/test.jpg")

# Convert to grayscale and apply Gaussian filtering
im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
# im_gray = cv2.medianBlur(im_gray, 15)
im_gray = cv2.GaussianBlur(im_gray, (5, 5), 0)

# Threshold the image
im_th = cv2.threshold(im_gray, 130, 255, cv2.THRESH_BINARY_INV)[1]

# Find contours in the image
# https://docs.opencv.org/4.0.0/dd/d49/tutorial_py_contour_features.html
if cv2.__version__ == '4.0.0':
    ctrs = cv2.findContours(im_th.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[0]
# https://docs.opencv.org/3.4.2/dd/d49/tutorial_py_contour_features.html
else:
    ctrs = cv2.findContours(im_th.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[1]

# Get rectangles contains each contour if area > 1200 (digits case)
rects = []
image_list = None
contours_list = []
counter = 0
for ctr in ctrs:
    # Previous area ==> 1200
    if cv2.contourArea(ctr) > 4000:
        x, y, w, h = cv2.boundingRect(ctr)
        digit = im_th[y:y + h, x:x + w]

        # Make a list of contours and bounding rect ==> Used to sort in an order
        z = x + y
        contours_list.append([counter, z, [x, y]])
        counter += 1

        # For each rectangular region, calculate HOG features and predict
        # Make rectangle region around the digit
        cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 3)

        # Resize the image
        roi = cv2.resize(digit, (18, 18))
        # Center and add border to the image
        roi = cv2.copyMakeBorder(roi, 5, 5, 5, 5, cv2.BORDER_CONSTANT, value=0)

        newImage = np.array(roi)
        newImage = newImage.flatten()

        newImage = newImage.reshape(1, -1)

        if image_list is None:
            image_list = newImage
        else:
            image_list = np.concatenate((image_list, newImage))

# Sort contours list
sorted_contours_list = sorted(contours_list, key=operator.itemgetter(1), reverse=False)
# Create list of sorted contours indices
sorted_contours_list_index = []
contours_positions = []
for temp in range(len(sorted_contours_list)):
    sorted_contours_list_index.append(sorted_contours_list[temp][0])
    contours_positions.append(sorted_contours_list[temp][2])

# Create sorted image list
sorted_image_list = []
for temp in sorted_contours_list_index:
    sorted_image_list.append(image_list[temp, :])
sorted_image_list = np.array(sorted_image_list)

obj = TF_learn_cnn_mnist.TF_learn_cnn_mnist()
ans1 = obj.test(sorted_image_list, reuse=True)
print(ans1)

for temp in range(len(sorted_image_list)):
    digit_image = np.reshape(sorted_image_list[temp, :], (28, 28))
    cv2.imwrite(str(temp) + ".jpg", digit_image)

for temp in range(len(ans1)):
    contour_x, contour_y = contours_positions[temp]
    cv2.putText(im, str(ans1[temp]), (contour_x, contour_y), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 255))

# Display student information start
predicted_label = []
for val in ans1:
    predicted_label.append(str(val))

from scipy.spatial import distance
f = open("data/student_information.txt", 'r')
distance_list = []
student_list = []
for line in f:
    student_id = line.split(",")[1].strip()
    student_list.append(line.split(","))
    id = []
    for temp1 in student_id:
        id.append(str(temp1))

    # Hamming distance gives no. of different values. So do -1
    distance_list.append(1-distance.hamming(predicted_label, id))

predicted_id = np.argmax(distance_list)
print("=================================")
# Converts to integer
print("Student name: ", student_list[int(predicted_id)][0])
print("=================================")
# Display student information start

cv2.imwrite("Resulting Image with Rectangular ROIs.jpg", im)
# cv2.imshow("Gray image", im_gray)
cv2.imwrite("Threshold image.jpg", im_th)

k = cv2.waitKey(0)
if k == 27:
  sys.exit()
